import React, { Component } from 'react'
import LogoPlay from '../images/play-icon.png'
import LogoPlus from '../images/plus-icon.png'
export class CarouselItem extends Component {
    render(props) {
        console.log(this.props)
        const { cover ,title , year ,contentRating , duration } = this.props
        return (
            <React.Fragment>

                <div className="carousel-item">
                    <img className="carousel-item__img"
                        src={cover}
                        alt={title}
                        title={title}
                         />
                    <div className="carousel-item__details">
                        <div>
                            <img className="carousel-item__details--img" src={LogoPlay} alt="Play Icon" />
                            <img className="carousel-item__details--img" src={LogoPlus} alt="Plus Icon" />
                        </div>
                        <p className="carousel-item__details--title">{title}</p>
                        <p className="carousel-item__details--subtitle">{year} {contentRating} {duration} minutes</p>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default CarouselItem