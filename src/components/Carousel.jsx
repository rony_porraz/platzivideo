import React, { Component } from 'react'
import CarouselItem from './CarouselItem'

export class Carousel extends Component {
    render(props) {
        return (
            <section className="carousel">
                <div className="carousel__container">
                    {this.props.children}
                </div>
            </section>
        )
    }
}

export default Carousel
