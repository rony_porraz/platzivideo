import React, { Component } from 'react'
import './styles/Search.scss'

export class Search extends Component {
    render() {
        return (
            <React.Fragment>
             <section className="main">
                 <h2 className="main__title">¿Qué quieres ver hoy?</h2>
                 <input type="text" className="input" placeholder="Buscar..." />
             </section>
            </React.Fragment>
        )
    }
}

export default Search
