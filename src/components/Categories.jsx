import React , { Component } from 'react';
import './styles/Categories.scss'
import Carousel from './Carousel';

export class Categories extends Component {
  render(props) {

  return (
  <React.Fragment>
    <div className="main-margin">
      <h3 className="categories__title">{this.props.title}</h3>
      {this.props.children}    
    </div>
  </React.Fragment>
  )
}}
export default Categories
