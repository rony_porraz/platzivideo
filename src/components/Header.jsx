import React, { Component } from 'react'
import './styles/header.scss'
import PlatziLogo from '../images/logo-platzi-video.png'
import UserIcon from '../images/user-icon.png'

export class Header extends Component {
    render() {
        return (
            <React.Fragment>
                <header className="header">
                    <img className="header__img" src={PlatziLogo} alt="Platzi Video" />
                        <div className="header__menu">
                            <div className="header__menu--profile">
                                <img src={UserIcon} alt="Icon User" />
                                    <p>Perfil</p>
                            </div>
                                <ul>
                                    <li><a href="/">Cuenta</a></li>
                                    <li><a href="/">Cerrar Sesión</a></li>
                                </ul>
                            </div>
                </header>
            </React.Fragment>
        )
    }
}

export default Header
