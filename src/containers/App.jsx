// LIBRARIES
import React, { useState, useEffect } from 'react';

// COMPONENTS
import Header from '../components/Header';
import Search from '../components/Search';
import Categories from '../components/Categories';
import Carousel from '../components/Carousel';
import CarouselItem from '../components/CarouselItem';
import Footer from '../components/Footer';

// API
import useinitialState from '../hooks/useInitialState'

// STYLES
import './styles/App.scss'


const App = () => {

  const videos = useinitialState('http://localhost:3000/initialState')

  //console.log(videos)
  return (
    <React.Fragment>
      <Header />
      <Search />

      { // SI ES MAYOR A 0 LISTA
        videos.mylist, length > 0 &&
        <Categories title="Mi lista">
          <Carousel>
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
            <CarouselItem />
          </Carousel>
        </Categories>

      }




      <Categories title="Trends">
        <Carousel>
          {videos.trends.map(item =>
            <CarouselItem key={item.id} {...item} />
           )
          }
        </Carousel>
      </Categories>


      <Categories title="Originals">
        <Carousel>
        {videos.originals.map(item =>
            <CarouselItem key={item.id} {...item} />
           )
          }
        </Carousel>
      </Categories>

      <Footer />
    </React.Fragment>
  );
}

export default App;
